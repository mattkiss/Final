import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.sql.ResultSetMetaData;

public class TestGit {

	public static void main(String[] args) throws SQLException {
		System.out.println("Hello!");
		Connection  connection = DriverManager.getConnection("jdbc:derby:test;create=true;");
		Statement stmt = connection.createStatement();
		stmt.executeUpdate("CREATE TABLE b (id int not null primary key, value varchar(20))");
		stmt.executeUpdate("insert into b (id) values(7)");
		ResultSet rs = null;
		rs=stmt.executeQuery("select * from b");
		while (rs.next()) {
            System.out.printf("%d",
            rs.getInt(1));
         }
		//System.out.println(stmt.executeQuery("select * from b").next());
		//stmt.executeUpdate("CREATE USER 'javaTest'@'%' IDENTIFIED BY ''");
		//stmt.close();
		//connection.close();
	}

}